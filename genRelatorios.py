# a stacked bar plot with errorbars
import numpy as np
import matplotlib.pyplot as plt

def bar(values):
    N = len(values)
    ind = np.arange(N)    # the x locations for the groups
    width = 0.4       # the width of the bars: can also be len(x) sequence
    for i, val in enumerate(values):
        plt.bar(i, val, width, align='center')
    plt.ylabel('Tempo')
    plt.title('Comparativo do Tempo de execucao dos Algoritimos')
    plt.xticks(ind, ('SelectionSort' , 'QuickSort'))
    # plt.yticks(np.arange(0, max(values)))
    print('[SelectionSort,\t\tQuickSort]')
    print(values)
    plt.show()
    

# showRelBar([0.3, 0.5])