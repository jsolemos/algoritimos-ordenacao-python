import time, sys
import selectionSort
import genRelatorios
import quicksort
import matplotlib.pyplot as plt

relatorio=[]
n=10
if len(sys.argv) > 1:
    n=int(sys.argv[1])

file='dados/'+str(n)+'-arq.txt'

try:
    arquivo = open(file, 'r')
    lista = arquivo.read().split(';')
    arquivo.close()
except expression as identifier:
    print("Arquivo nao encontrado.")

selInit = time.time()
selectionSort.call(lista[:])
selEnd = time.time()
relatorio.append(selEnd-selInit)

quickInit = time.time()
quicksort.quick_sort(lista[:], 0, (len(lista)-1))
quickEnd = time.time()
relatorio.append(quickEnd-quickInit)



# exibe relatório
genRelatorios.bar(relatorio)