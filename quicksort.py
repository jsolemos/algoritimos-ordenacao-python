import random

def randomized_partition(A, p, r):
    i = random.randint(p,r)
    aux = A[i]
    A[i]=A[r]
    A[r]=aux
    q = partition(A, p, r)
    return q

def randomized_quick_sort(A, p, r):
    if (p < r):
        q = randomized_partition(A, p,r)
        randomized_quick_sort(A, p, q-1)
        randomized_quick_sort(A, q+1, r)


def quick_sort(A, p, r):
    if (p < r):
        q = partition(A, p,r)
        quick_sort(A, p, q-1)
        quick_sort(A, q+1, r)

def partition(A, p, r):
    x = A[r]
    # print("Pivo: ", x)
    i = p-1
    j = p
    while (j <= r-1):
        if A[j] and x and int(A[j]) <= int(x):
            i = i+1
            aux = A[i]
            A[i]=A[j]
            A[j]=aux
        j=j+1
    
    # print("I: ",i)
    aux = A[i+1]
    A[i+1] = A[r]
    A[r] = aux
    return i+1


# lista = []
# for i in range(5):
#         lista.append(random.randint(0,100))
# print("LISTA DESORDENADA")
# for i in range(5):
#         print(lista[i])
# randomized_quick_sort(lista, 0, 4)
# print("ORDENACAOO QUICK-SORT")
# for i in range(5):
#         print(lista[i])

