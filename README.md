# Comparativo de tempo de execução de algoritimos de ordenação

## Instalação de dependências
    $ pip install -r requirements.txt

### Gerar arquivo com números aleatórios
Já existem arquivos lista com 10, 100, 200, 300, 1.000, 10.000 e 100.000 números gerados.

    $ python genfile.py 100 # gera um arquivo com 100 números aleatórios
o valor **100** no comando acima é referente a quantidade de números que deseja gerá no arquivo.

### Executa arquivo com números aleatórios

    $ python exec.py 100 # executa os algoritimos e exibe o gráfico comparativo do tempo de execução

o valor **100** no comando acima é referente ao arquivo que contém a quantidade especificada de números gerado.