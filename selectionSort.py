import sys
# A = [64, 25, 12, 22, 11]
def call(lista):
    for i in range(len(lista)):
        
        # Find the minimum element in remaining 
        # unsorted array
        min_idx = i
        for j in range(i+1, len(lista)):
            if lista[min_idx] and lista[j] and int(lista[min_idx]) > int(lista[j]):
                min_idx = j
                
        # Swap the found minimum element with 
        # the first element        
        lista[i], lista[min_idx] = lista[min_idx], lista[i]
    
    # Driver code to test above
    # print (lista)
    # for i in range(len(A)):
        # print("%d" %A[i]), 

# selectionSort(A)